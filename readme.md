# Github Repositories

[![Screen recording](https://img.youtube.com/vi/oTWBgbVQn4c/0.jpg)](https://youtu.be/oTWBgbVQn4c   "Screen recording")


## TODOs

- [x] Implement Network Layer
- [x] Implement CoreData stack
- [x] Implement SyncManager
- [x] Implement CollectionViewViewModel
- [x] Implement CollectionViewCellViewModel
- [x] Implement CollectionViewCell

## Architecture

![Architecture](images/architecture.svg)