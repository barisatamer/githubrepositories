//
//  DownloadTimeEstimater.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 20/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import UIKit

class DownloadTimeEstimater {
    
    // Singleton
    static let shared = DownloadTimeEstimater()
    private init() {}
    
    private var start: Date?
    var currentSpeed: Double?
    
    func startMeasure() {
        start = Date()
    }
    
    func endMeasure(dataCount: Int) {
        guard let start = start else { return }
        let elapsed = CGFloat(Date().timeIntervalSince(start))
        currentSpeed = Double(dataCount) / Double(elapsed) / 1024
        print("✅ Downloaded: \(dataCount) bytes. Speed ⚡️: \(self.currentSpeed ?? 0 ) kbytes/sec")
    }
    
    func calculateEstimatedDownloadSize(forKBs: Int64) -> Double {
        if let speed = currentSpeed {
            return Double(forKBs) / speed
        } else {
            return 0
        }
    }
    
}
