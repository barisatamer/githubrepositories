//
//  Result.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 19/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import Foundation

enum Result<T, U: Error> {
    case success(payload: T)
    case failure(U?)
}
