//
//  Repository+Extension.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 19/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import CoreData

extension Repository {
    
    /// Helper for creating RepositoryEntity(NSManagedObject) from Repository model
    ///
    /// - Parameter context: NSManagedObjectContext
    /// - Returns: Returns an NSManagedObject
    @discardableResult
    func managedObject(forContext context: NSManagedObjectContext) -> RepositoryEntity {
        let entity = RepositoryEntity(context: context)
        entity.name = self.name
        entity.fileSize = Int64(self.size)
        entity.fullName = self.fullName
        entity.ownerName = self.owner.login
        entity.hasWiki = self.hasWiki
        entity.serverId = Int64(self.id)
        entity.starCount = Int64(self.stargazersCount)
        return entity
    }
}
