//
//  UIViewController+Extension.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 23/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Presents an UIAlertcontroller with one cancel button & title
    ///
    /// - Parameters:
    ///   - title: Title
    ///   - message: Message
    func showAlert(title: String, message: String) {
        let alert = UIAlertController.alert(title: title, message: message)
        present(alert, animated: true, completion: nil)
    }
}

extension UIAlertController {
    
    /// Creates an UIAlertController with one cancel button & title
    ///
    /// - Parameters:
    ///   - title: Title
    ///   - message: Message
    /// - Returns: UIAlertController
    class func alert(title: String, message: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelButton = UIAlertAction(title: "Okay", style: .cancel) { (action) in }
        alert.addAction(cancelButton)
        return alert
    }
}
