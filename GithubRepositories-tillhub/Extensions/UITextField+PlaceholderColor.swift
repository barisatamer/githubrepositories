//
//  UITextField+PlaceholderColor.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 20/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import UIKit

extension UITextField {

    func setPlaceholder(_ string: String) {
        self.attributedPlaceholder = NSAttributedString(string: string,
                           attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        
    }
}
