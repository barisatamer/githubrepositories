//
//  CollectionViewCell.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 19/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelRepositoryName: UILabel!
    @IBOutlet weak var labelOwnerName: UILabel!
    @IBOutlet weak var labelRepositorySize: UILabel!
    @IBOutlet weak var labelEstimatedDownloadTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func set(viewModel: CollectionViewCellViewModel) {
        labelRepositoryName.text = viewModel.repoName
        labelOwnerName.text = viewModel.userName
        labelRepositorySize.text = viewModel.repositorySize
        labelEstimatedDownloadTime.text = "Estimated download time: \(viewModel.estimatedDownloadTime)"
        containerView.backgroundColor = viewModel.bgColor
    }
}
