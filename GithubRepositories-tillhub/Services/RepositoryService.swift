//
//  RepositoryService.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 19/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import Foundation

enum GithubFailureReason: Error {
    case fail(String)
}
extension GithubFailureReason: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .fail(let msg):
            return msg
        }
    }
}

final class RepositoryService {
    
    typealias GetRepositoriesResult = Result<RepositoryResult, GithubFailureReason>
    typealias GetRepositoriesCompletion = (_ result: GetRepositoriesResult) -> Void

    /// Fetches the repositories at Github, search/repositories?q={QUERY}
    ///
    /// - Parameters:
    ///   - text: Search text
    ///   - completion: Completion closure, contains RepositoryResult
    func getRepositories(text: String, perPage: Int = 10, page: Int = 1, completion: @escaping GetRepositoriesCompletion) {
        let endPoint = C.Networking.SearchEndPoint.replacingOccurrences(of: "{QUERY}", with: text)
            .replacingOccurrences(of: "{PER_PAGE}", with: String(perPage))
            .replacingOccurrences(of: "{PAGE}", with: String(page))
        
        guard let url = URL(string: endPoint) else { return }
        
        let request = URLRequest(url: url)
        DownloadTimeEstimater.shared.startMeasure()
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let `error` = error {
                completion(.failure(GithubFailureReason.fail(error.localizedDescription)))
                return
            }
            
            guard let `data` = data else {
                completion(.failure(GithubFailureReason.fail("Data is null")))
                return
            }
            
            DownloadTimeEstimater.shared.endMeasure(dataCount: data.count)
            let decoder = JSONDecoder()
            do {
                let repositoryResult = try decoder.decode(RepositoryResult.self, from: data)
                completion(.success(payload: repositoryResult))
            } catch {
                completion(.failure(GithubFailureReason.fail(error.localizedDescription)))
            }
            
        }.resume()
        
    }
}
