//
//  ViewController.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 19/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    // MARK: -IBOutlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    // MARK: - Private Methods
    
    let cellIdentifier = "CollectionViewCell"
    private var blockOperations: [BlockOperation] = []
    private var viewModel = CollectionViewViewModel()
    
    
    // MARK: - View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register cell for collection view
        let cellNib = UINib(nibName: "CollectionViewCell", bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: cellIdentifier)
        
        // Set list layout, for vertical list appearance
        collectionView.setCollectionViewLayout(ListFlowLayout(), animated: false)
        
        // Set the fetched results delegate
        viewModel.fetchedResultsController.delegate = self
        viewModel.delegate = self
        
        // Fetching
        viewModel.fetchFromCoreData()
        
        // Search & Save using SyncManager
        viewModel.searchAndSaveGithub(forText: "tetris")
        
        textFieldSearch.setPlaceholder("Search Repositories")
    }
    
    @IBAction func textFieldDidEndOnExit(textField: UITextField) {
        view.endEditing(true)
        if let searchString = textField.text {
            viewModel.searchAndSaveGithub(forText: searchString)
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
    }
}


// MARK: - UICollectionViewDataSource

extension ViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? CollectionViewCell else {
            fatalError("Can not dequeued!")
        }
        
        if let cellViewModel = viewModel.cellViewModel(forIndexPath: indexPath) {
            cell.set(viewModel: cellViewModel)
        }
        
        return cell
    }
}


// MARK: - NSFetchedResultsControllerDelegate

extension ViewController: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.performBatchUpdates({
            self.blockOperations.forEach { $0.start() }
        }) { (finished) in
            self.blockOperations.removeAll()
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            guard let newIndexPath = newIndexPath else { break }
            blockOperations.append(BlockOperation(block: { [weak self] in
                if let `self` = self {
                    self.collectionView.insertItems(at: [newIndexPath])
                }
            }))
            
        case .delete:
            guard let indexPath = indexPath else { break }
            blockOperations.append(BlockOperation(block: { [weak self] in
                if let `self` = self {
                    self.collectionView.deleteItems(at: [indexPath])
                }
            }))
        case .update:
            guard let indexPath = indexPath else { break }
            blockOperations.append(BlockOperation(block: { [weak self] in
                if let `self` = self {
                    self.collectionView.reloadItems(at: [indexPath])
                }
            }))
        case .move:
            break;
        }
    }
}

// MARK: - UIScrollViewDelegate

extension ViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let distance = scrollView.contentSize.height - (targetContentOffset.pointee.y + scrollView.bounds.height)
        
        if !viewModel.loading && distance < 200 {
            guard let searchString = self.textFieldSearch.text else { return }
            viewModel.loading = true
            viewModel.searchAndSaveGithub(forText: searchString)
            activityIndicator.startAnimating()
        }
    }
}


// MARK: - CollectionViewViewModelDelegate

extension ViewController: CollectionViewViewModelDelegate {
    func loadingDidFinished(_ error: Error?) {
        if let error = error {
            showAlert(title: "❌", message: error.localizedDescription)
        }
        
        activityIndicator.stopAnimating()
    }
}
