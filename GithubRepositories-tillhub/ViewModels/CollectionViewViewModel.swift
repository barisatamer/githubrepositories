//
//  CollectionViewViewModel.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 19/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import UIKit
import CoreData

protocol CollectionViewViewModelDelegate: class {
    func loadingDidFinished(_ error: Error?)
}

final class CollectionViewViewModel {
    
    private let coreDataManager = CoreDataManager(modelName: C.Storage.ModelName)
    
    private lazy var syncManager: SyncManager = {
        return SyncManager.init(coreDataManager: coreDataManager)
    }()
    
    public lazy var fetchedResultsController: NSFetchedResultsController<RepositoryEntity> = {
        let fetchRequest: NSFetchRequest<RepositoryEntity> = RepositoryEntity.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(RepositoryEntity.starCount), ascending: false)]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.coreDataManager.mainMOC, sectionNameKeyPath: nil, cacheName: nil)
        return fetchedResultsController
    }()
    
    var currentPage = 1
    var currentSearchString: String?
    var loading = false
    weak var delegate: CollectionViewViewModelDelegate?
    
    func fetchFromCoreData() {
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    func searchAndSaveGithub(forText text: String) {
        if let currentSearchString = currentSearchString, currentSearchString != text {
            clearSearchResults()
        }
        if currentSearchString == nil {
            clearSearchResults()
        }
        
        syncManager.saveFor(text: text, perPage: 10, page: currentPage) { (error) in
            self.loading = false
            self.delegate?.loadingDidFinished(error)
            self.currentPage = self.currentPage + 1
        }
        
        currentSearchString = text
    }
    
    func clearSearchResults() {
        syncManager.clearSyncHistory()
    }
    
    
    func numberOfSections() -> Int {
        guard let sections = fetchedResultsController.sections else { return 0 }
        return sections.count
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        guard let section = fetchedResultsController.sections?[section] else { return 0 }
        return section.numberOfObjects
    }
    
    func cellViewModel(forIndexPath indexPath: IndexPath) -> CollectionViewCellViewModel? {
        let viewModel = fetchedResultsController.object(at: indexPath)
        return viewModel
    }
}
