//
//  CollectionViewCellViewModel.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 19/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import UIKit
import CoreData

protocol CollectionViewCellViewModel {
    var repoName: String { get }
    var userName: String { get }
    var estimatedDownloadTime: String { get }
    var bgColor: UIColor { get }
    var repositorySize: String { get }
}


extension RepositoryEntity: CollectionViewCellViewModel {
    var repoName: String {
        return self.fullName ?? ""
    }
    
    var userName: String {
        return self.ownerName ?? ""
    }
    
    var estimatedDownloadTime: String {
        let estimatedTime = DownloadTimeEstimater.shared.calculateEstimatedDownloadSize(forKBs: self.fileSize)
        return String(format: "%.01f seconds", estimatedTime)
    }
    
    var bgColor: UIColor {
        return hasWiki ? #colorLiteral(red: 0.9764705882, green: 0.9960784314, blue: 0.9803921569, alpha: 1) : .white
    }
    
    var repositorySize: String {
        return "\(self.fileSize) KB"
    }
    
    
}
