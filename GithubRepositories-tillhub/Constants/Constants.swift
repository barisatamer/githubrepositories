//
//  Constants.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 19/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import Foundation

enum C {
    enum Networking {
        static let SearchEndPoint = "https://api.github.com/search/repositories?q={QUERY}&per_page={PER_PAGE}&page={PAGE}&sort=stars&order=desc"
    }
    enum Storage {
        static let ModelName = "Repositories"
    }
}
