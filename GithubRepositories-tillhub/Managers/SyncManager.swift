//
//  SyncManager.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 19/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import Foundation
import CoreData

/// This class is responsible for fetching the Github repositories
/// and inserting the results into CoreData
final class SyncManager {
    
    var coreDataManager: CoreDataManager
    
    // MARK: Initializer
    
    init(coreDataManager: CoreDataManager) {
        self.coreDataManager = coreDataManager
    }
    
    typealias SyncCompletion = (Error?) -> Void
    
    func saveFor(text: String, perPage: Int = 2, page: Int = 0, completion: @escaping SyncCompletion) {
        
        RepositoryService.init().getRepositories(text: text, perPage: perPage, page: page) { (result) in
            
            switch result {
            case .success(let payload):
                print("✅ Successfully downloaded \(payload.repositories.count) repositories")
                
                // Update/Insert for each Repository item
                self.coreDataManager.performOnPrivate(operations: {(privateMOC) in
                    payload.repositories.forEach { self.upsert(model: $0, context: privateMOC) }
                }, onSave: { (hadChanges) in
                    DispatchQueue.main.async { completion(nil) }
                })
                
            case .failure(let err):
                print("\(err?.localizedDescription ?? "")")
                DispatchQueue.main.async { completion(err) }
            }
        }
    }
    
    
    /// Insert a new record or update the existing record
    ///
    /// - Parameters:
    ///   - model: Repository
    ///   - context: NSManagedObjectContext
    private func upsert(model: Repository, context: NSManagedObjectContext) {
        if let existingObject = existingObject(serverId: Int64(model.id), context: context) {
            let newObj = model.managedObject(forContext: context)
            existingObject.entity.attributesByName.forEach({ (key, value) in
                existingObject.setValue(newObj.value(forKey: key), forKey: key)
            })
            context.delete(newObj)
        } else {
            model.managedObject(forContext: context)
        }
    }
    
    
    /// Finds the managed object with server id, or returns nil
    ///
    /// - Parameters:
    ///   - serverId: Server id of the record
    ///   - context: NSManagedObjectContext
    /// - Returns: RepositoryEntity?
    private func existingObject(serverId: Int64, context: NSManagedObjectContext) -> RepositoryEntity? {
        let fetchRequest: NSFetchRequest<RepositoryEntity> = RepositoryEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "serverId = %d", serverId)
        do {
            return try context.fetch(fetchRequest).first
        } catch {
            return nil
        }
    }
    
    
    /// Deletes the RepositoryEntity records
    public func clearSyncHistory() {
        let privateMOC = coreDataManager.privateMOC

        let fetchRequest: NSFetchRequest<RepositoryEntity> = RepositoryEntity.fetchRequest()
        do {
            let results = try privateMOC.fetch(fetchRequest)
            results.forEach { privateMOC.delete($0) }
            self.coreDataManager.saveChanges()
        } catch {
            print("Failed to delete records \(error)")
        }
    }
}
