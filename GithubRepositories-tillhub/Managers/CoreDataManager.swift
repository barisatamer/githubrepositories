//
//  CoreDataManager.swift
//  GithubRepositories-tillhub
//
//  Created by Baris Atamer on 19/05/2018.
//  Copyright © 2018 ba. All rights reserved.
//

import CoreData

final class CoreDataManager {
    
    // MARK: - Private Properties
    
    private let modelName: String
    
    // Child context, mainQueueConcurrencyType, main queue, or the main thread of the application
    // or viewContext like how NSPersistentContainer uses
    private(set) lazy var mainMOC: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.parent = self.privateMOC
        managedObjectContext.automaticallyMergesChangesFromParent = true
        return managedObjectContext
    }()
    
    // Parent context, works on backround
    private(set) lazy var privateMOC: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        return managedObjectContext
    }()
    
    private lazy var managedObjectModel: NSManagedObjectModel = {
        guard let modelURL = Bundle.main.url(forResource: self.modelName, withExtension: "momd") else {
            fatalError("Data model could not be found")
        }
        
        // Init Managed Object Model
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Unable to Load Data Model")
        }
        
        return managedObjectModel
    }()
    
    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // Initialize Persistent Store Coordinator
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        
        // Helpers
        let fileManager = FileManager.default
        let storeName = "\(self.modelName).sqlite"
        
        // URL Documents Directory
        let documentsDirectoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        // URL Persistent Store
        let persistentStoreURL = documentsDirectoryURL.appendingPathComponent(storeName)
        print("💾 persistentStoreURL: \(persistentStoreURL)")
        
        do {
            // Add Persistent Store
            let options = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: persistentStoreURL, options: options)
        } catch {
            fatalError("Unable to Add Persistent Store")
        }
        
        return persistentStoreCoordinator
    }()
    
    
    // MARK: - Initialization
    
    init(modelName: String) {
        self.modelName = modelName
    }
    

    // MARK: - Public Methods
    
    public func saveChanges() {
        // Save the child context(mainMOC),
        // and wait till the changes are pushed to the parent context(privateMOC)
        mainMOC.performAndWait {
            do {
                if self.mainMOC.hasChanges {
                    try self.mainMOC.save()
                }
            } catch {
                let saveError = error as NSError
                print("Unable to save changes of main managed object context")
                print("\(saveError), \(saveError.localizedDescription)")
            }
        }
        
        privateMOC.perform {
            do {
                if self.privateMOC.hasChanges {
                    try self.privateMOC.save()
                }
            } catch {
                let saveError = error as NSError
                print("Unable to save changes of Private Managed Object")
                print("\(saveError), \(saveError.localizedDescription)")
            }
        }
        
    }
    
    public func performOnPrivate(operations: @escaping (_ privateContext: NSManagedObjectContext) -> Void, onSave: @escaping (_ hadChanges: Bool) -> Void ) {
        privateMOC.perform {
            operations(self.privateMOC)
            do {
                if self.privateMOC.hasChanges {
                    try self.privateMOC.save()
                }
                onSave(self.privateMOC.hasChanges)
            } catch {
                let saveError = error as NSError
                print("Unable to save changes of Private Managed Object")
                print("\(saveError), \(saveError.localizedDescription)")
            }
        }
    }
    
}
